//==================================================================
  // MODEL -----------------------------------------------------
//==================================================================
var model = ( () => {


  const inventory = {
      meat: {
          steak: [
              {
                  id: 1,
                  type: 'Ribeye Steak',
                  description: 'The rib eye or ribeye is a beef steak from the rib section. The rib section of beef spans from ribs six through twelve. Ribeye steaks are mostly composed of the longissimus dorsi muscle but also contain the complexus and spinalis muscles.',
                  price: 2.99,
                  unit: 'lb',
                  img: 'ribeye.jpeg'
              },
              {
                  id: 2,
                  type: 'Flank',
                  description: 'Flank steak is a cut of beef taken from the abdominal muscles or lower chest of the steer. French butchers refer to it as bavette, which means "bib". Similarly, it is known in Brazil as fraldinha.',
                  price: 1.99,
                  unit: 'lb',
                  img: 'flank.jpg'
              },
              {
                  id: 3,
                  type: 'Filet Mignon',
                  description: 'Filet mignon is a steak cut of beef taken from the smaller end of the tenderloin, or psoas major of the cow carcass, usually a steer or heifer. In French this cut is always called filet de bœuf as filet mignon refers to pork tenderloin.',
                  price: 7.99,
                  unit: 'lb',
                  img: 'filetmignon.jpeg'
              }
          ],
          pork: [
              {
                  id: 4,
                  type: 'Pork Chops',
                  description: 'A pork chop, like other meat chops, is a loin cut taken perpendicularly to the spine of the pig and usually containing a rib or part of a vertebra.',
                  price: 4.59,
                  unit: 'lb',
                  img: 'porkchops.jpg'
              },
              {
                  id: 5,
                  type: 'Pork Loin',
                  description: 'Pork loin is a cut of meat from a pig, created from the tissue along the dorsal side of the rib cage.',
                  price: 10.99,
                  unit: 'lb',
                  img: 'porkloin.jpg'
              }                
          ],
          chicken: [
              {
                  id: 6,
                  type: 'Chicken Breast',
                  description: 'Chicken breasts are tossed in a simple herb mixture then oven baked until they are tender and juicy! This easy baked chicken recipe makes chicken breasts that are lightly seasoning and great to top',
                  price: 3.59,
                  unit: 'lb',
                  img: 'chicken_breast.jpg'
              },
              {
                  id: 7,
                  type: 'Chicken Wings',
                  description: 'Crispy Buffalo Chicken Wings are exactly what they claim to be. CRISPY without deep frying, using one special ingredient you have in your stomach',
                  price: 3.59,
                  unit: 'doz',
                  img: 'chicken_wings.jpg'
              },
              {
                  id: 8,
                  type: 'Whole Chicken',
                  description: 'his roast chicken recipe eliminates one of the most common complaints about whole roast chickens—that its hard to know when they\'re cooked all the way',
                  price: 12.99,
                  unit: 'per',
                  img: 'slow-cooker-whole-chicken-3.jpg'
              }
          ]
      },
      bakery: {
          bread: [
              {
                  id: 9,
                  type: 'White Bread',
                  description: 'White bread typically refers to breads made from wheat flour from which the bran and the germ layers have been removed from the whole wheatberry as part of the flour grinding or milling process, producing a light-colored flour.',
                  price: 2.99,
                  unit: 'loaf',
                  img: 'white-bread.jpg'
              },
              {
                  id: 10,
                  type: 'Wheat Bread',
                  description: 'Whole wheat bread or wholemeal bread is a type of bread made using flour that is partly or entirely milled from whole or almost-whole wheat grains, see whole-wheat flour and whole grain. It is one kind of brown bread. ',
                  price: 3.99,
                  unit: 'loaf',
                  img: 'wheat-bread.jpg'
              }
          ], 
          bagels: [
              {
                  id: 11,
                  type: 'Everything Bagels',
                  description: 'An everything bagel is a popular type of bagel, allegedly invented by David Gussin, that includes a large variety of toppings;',
                  price: 4.99,
                  unit: 'doz',
                  img: 'everything_bagel.jpg'
              },
              {
                  id: 12,
                  type: 'Sesame Bagels',
                  description: 'There are 276 calories in 1 regular Bagel. Calorie breakdown: 9% fat, 76% carbs, 15% protein',
                  price: 4.99,
                  unit: 'doz',
                  img: 'sesame-bagel.jpg'
              }
          ]                 
      },
      produce: {
          fruit: [
              {
                  id: 13,
                  type: 'Oranges',
                  description: 'The orange is the fruit of the citrus species Citrus × sinensis in the family Rutaceae. It is also called sweet orange, to distinguish it from the related Citrus × aurantium, referred to as bitter orange. ',
                  price: 2.99,
                  unit: 'doz',
                  img: 'orange.jpg'
              },
              {
                  id: 14,
                  type: 'Kiwi',
                  description: 'Kiwifruit (often abbreviated as kiwi), or Chinese gooseberry, is the edible berry of several species of woody vines in the genus Actinidia.',
                  price: 1.99,
                  unit: 'doz',
                  img: 'kiwi.jpg'
              }
          ], 
          vegetable: [
              {
                  id: 15,
                  type: 'Lettuce',
                  description: 'Lettuce is an annual plant of the daisy family, Asteraceae. It is most often grown as a leaf vegetable, but sometimes for its stem and seeds. Lettuce is most often used for salads, although it is also seen in other kinds of food,',
                  price: 2.99,
                  unit: 'head',
                  img: 'lettuce.jpg'
              }
          ]
      }
  }

  const getInventory = () => {
    return inventory;
  }

  const getSingleItem = (id, cat, obj) => {
    let mainCat = obj[cat];
    let foundItem;
    for (const key in mainCat) {
      if (mainCat.hasOwnProperty(key)) {   
            mainCat[key].forEach((element) => {
              if(element.id === id) {
                foundItem = element;
            }
        });   
      }
  }
    return foundItem;
  }
  
  // RETURN model functions
  return {
      getInventory,
      getSingleItem
  }

})(); 

//==================================================================
  // VIEW -----------------------------------------------------
//==================================================================
var view = (() => {
    const showItem = (item) => {
        const qty = 1;
        const itemOutput = document.getElementById('item-output');
        itemOutput.innerHTML = '';
        const cardContainer = document.createElement('div');
        cardContainer.id = item.id;
        cardContainer.className = "card p-5 single-item"
        let content = `
        <h4>${item.type} </h4>
        <h5>$<span>${item.price} <em>${item.unit}</em></span></h5>
        <p>${item.description}</p>
        <img src="img/${item.img}">
        <br>
        <button data-id="${item.id}" data-price="${item.price}" data-type="${item.type}" data-qty="${qty}" id="add-item" class="btn btn-primary">Add To Cart</button>
        `;
        cardContainer.innerHTML = content;
        itemOutput.appendChild(cardContainer);
    }

 var showTotals = (subTotal, total) => {
    const subtotalHTML = document.getElementById('subtotal');
    subtotalHTML.textContent = `$${subTotal}`
    const totalHTML = document.getElementById('total');
    totalHTML.textContent = `$${total}`
 }

 var addToTable = (arr) => {
    document.getElementById('status').innerHTML = '';
    let content = '';
    arr.forEach(element => {
        content += `
        <tr data-tableid='${element.dataId}'>
            <th class="item-name">${element.name}</th>
            <td class="item-price">${element.price}</td>
            <td class="item-qty">${element.qty}</td>
            <th class="inc-item"> + </th>
            <th class="dec-item"> - </th> 
            <th class="delete-item"> x </th>     
            </tr>
        <tr>
    `;   
 });
    const tableBody = document.getElementById('table-body');
    tableBody.innerHTML = content;
 }

  // RETURN view functions
  return {
    showItem,
    showTotals,
    addToTable
  }
})();



//==================================================================
  // CONTROLLER ----------------------------------------------------
//==================================================================

var controller = ((modelCtl, viewCtl) => {

  let checkOutState = [];
  const inventory = modelCtl.getInventory();

  
  const getSingleItem = (e) => {
    e.preventDefault();
    if(e.target.classList.contains('view-btn')) {
        let id = parseInt( e.target.dataset.id);
        let cat = e.target.dataset.cat;
        const foundItem = modelCtl.getSingleItem(id, cat, inventory);
        viewCtl.showItem(foundItem);
    }
  }

 
  const getOuterMostCategory = (obj, catType) => {
      return obj[catType];
  }


  const getCategoryItems = (obj, type, category) => {
      let content = ``;
      type.forEach(element => {
        obj[element].map((item) => {
          content += `
          <li id="${item.id}" class="list-group-item ">
            <h5>${item.type}
              <span><button data-cat="${category}" data-id="${item.id}" class="btn btn-primary btn-sm float-right view-btn">View</button></span>
            </h5>
          </li>`
        })    
      });

      return {
        category: category,
        html: content
      }  
  }


  const addToCart = (e) => {
    if(e.target.id === 'add-item') {
        e.target.disabled = true;
        const itemObject = {};
        itemObject.name = e.target.dataset.type
        itemObject.price = parseFloat(e.target.dataset.price);
        itemObject.dataId = e.target.dataset.id;
        itemObject.qty = parseInt(e.target.dataset.qty);
        const idExists = checkifExists(itemObject.dataId, checkOutState);
        document.getElementById('checkout-btn').disabled = false;
        // FUNCTIONTO CHECK TO SEE IF ALREADY IN ARRAY-----------
        if(checkOutState.length === 0) {
            checkOutState.push(itemObject);
        } 
        else if(idExists) {
            // CHECK IF ITEM ALREADY EXISTS IN ARRAY
            console.log('Item Incremented');    
        } else {
            // PUSH NEW ITEM TO ARRAY 
            checkOutState.push(itemObject);
        }

        // ADD TO TABLE
        viewCtl.addToTable(checkOutState);
        // CALCULATE SUBTOTAL
        const subtotal = getSubTotal(checkOutState);
        // CALCULATE TOTAL WITH TAX ===== NEED TO FINISH
        const total = getTotal(subtotal);
        // FILL VIEW WITH TABLE ITEMS
        viewCtl.showTotals(subtotal, total);
    }
  }

  const checkifExists = (id, arr) => {
    let found = false;
    for (let index = 0; index < arr.length; index++) {
       if(arr[index].dataId === id) {
        arr[index].qty++;
        found = true;
       }      
    }
    return found;
}


  const updateQty = (e) => {
    const type = e.target.className;
    const id = e.target.parentElement.dataset.tableid;
    const updateObject = {};
    updateObject.type = type;
    updateObject.id = id;
    changeQty(updateObject, checkOutState);
  }

  const changeQty = (obj, arr) => {
    if(obj.type === 'item-name') {
        return false;
    }
    if(obj.type === 'inc-item') {
        arr.forEach(element => {
            if(obj.id === element.dataId) {
                element.qty++;
            }
        });
    }
    if(obj.type === 'dec-item') {
        arr.forEach(element => {
            if(obj.id === element.dataId) {
                if(element.qty === 0) {
                    return false;
                }
                element.qty--;
            }
        });
    } 
    if(obj.type === 'delete-item') {
       const updatedCart = arr.filter(element => obj.id !== element.dataId) 
        checkOutState = updatedCart;
        if(checkOutState.length === 0) {
            const status =  document.getElementById('status');
            status.textContent = 'No Items In Cart';
        }     
    }
    //RE RENDER --- TABLE ===================
    viewCtl.addToTable(checkOutState);
    const subtotal = getSubTotal(checkOutState);
    const total = getTotal(subtotal);
    viewCtl.showTotals(subtotal, total);
  }


  const getSubTotal = (arr) => {
    let subtotal = 0;
    arr.map(item => {
        subtotal += item.price * item.qty;
    })
    return subtotal.toFixed(2);
  }

  const getTotal = (num) => {
    // let tax = parseFloat((num * 0.05) * 1).toFixed(2);
    // console.log(tax);
    return num;
    // return final;
  }


  // ********** NEED TO FINISH *************
  const showCheckOutCart = (e) => {
    e.preventDefault();
  }
  // ********** NEED TO FINISH *************

  const meats = getOuterMostCategory(inventory, 'meat');
  const bakery = getOuterMostCategory(inventory, 'bakery');
  const produce = getOuterMostCategory(inventory, 'produce');

  const meatList = getCategoryItems(meats, ['steak', 'pork', 'chicken'], 'meat');
  const bakeryList = getCategoryItems(bakery, ['bread', 'bagels'], 'bakery');
  const produceList = getCategoryItems(produce, ['fruit', 'vegetable'], 'produce');



  // INITIAL PAGE LOAD LOAD ========================================================================
  const insert = document.getElementById('output');
  insert.innerHTML = `
    <div class="mt-1">
      <h3 class="text-center">${meatList.category}</h3>
      <ul class="list-group">
       ${meatList.html}
      </ul>  
    <div/>
    <div class="mt-1">
      <h3 class="text-center">${bakeryList.category}</h3>
      <ul class="list-group">
        ${bakeryList.html}
      </ul>
    <div>
    <div class="mt-1">
      <h3 class="text-center">${produceList.category}</h3>
      <ul class="list-group">
        ${produceList.html}
      </ul>
    <div>`
  // INITIAL PAGE LOAD LOAD ========================================================================


  // EVENT LISTENERS ==============================================================================
  var setUpEventListeners = () => {
      const allItems = document.getElementById('output');
      allItems.addEventListener('click', getSingleItem)
      const singleItem = document.getElementById('item-output');
      singleItem.addEventListener('click', addToCart);
      const tableBody = document.getElementById('table-body');
      tableBody.addEventListener('click', updateQty);
      const checkOutBtn = document.getElementById('checkout-btn');
      checkOutBtn.addEventListener('click', showCheckOutCart)
  };
  // EVENT LISTENERS ==============================================================================


  // RETURN controller functions
  return {
      init: () => {
          console.log('***** APP RUNNING *******');
          setUpEventListeners();
      }
  }

})(model, view);


controller.init();